/*
 * Copyright (c) 2018 Baidu.com, Inc. All Rights Reserved
 */
package com.baidu.personal

/**
 * Hello world!
 *
 * @author java-cmc
 * @version 1.0.0
 * @since 1.0.0
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
